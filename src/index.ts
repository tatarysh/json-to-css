const isObject = (value: unknown): value is Record<string, unknown> => {
  return !Array.isArray(value) && typeof value === 'object'
}

const objectToCss = (object: Record<string, unknown>, parents: string[]): string[] => {
  const accumulator: string[] = []
  const simple: string[] = []

  for (const [key, value] of Object.entries(object)) {
    if (isObject(value)) {
      if (key.startsWith('&')) {
        const clonedParents = [...parents]

        const selectors = (clonedParents.pop() as string)
          .split(',')
          .map((s) => `${s.trim()}${key.substring(1)}`)
          .join(',')

        accumulator.push(...objectToCss(value, [...clonedParents, selectors]))
      } else if (key.startsWith('@')) {
        accumulator.push(`${key}{${jsonToCss(value, parents.join(' '))}}`)
      } else {
        const minimalized = key
          .split(',')
          .map((s) => s.trim())
          .join(',')

        accumulator.push(...objectToCss(value, [...parents, minimalized]))
      }
    } else {
      simple.push(`${key}:${value}`)
    }
  }

  if (simple.length) {
    accumulator.push(`${parents.join(' ')}{${simple.join(';')}}`)
  }

  return accumulator
}

const jsonToCss = (value: Record<string, unknown>, selector: string): string => {
  return objectToCss(value, [selector]).reverse().join('')
}

export default jsonToCss

const pkg = require('./package.json')
const typescript = require('@rollup/plugin-typescript')
const { minify } = require('rollup-plugin-esbuild-minify')

module.exports = {
  input: 'src/index.ts',
  plugins: [typescript(), minify()],
  output: [
    {
      name: 'json-to-css',
      file: pkg.browser,
      format: 'umd',
    },
    {
      file: pkg.module,
      format: 'es',
    },
  ],
}

import jsonToCss from '../src/index'

test('Expect empty css from an empty JSON', () => {
  expect(jsonToCss({}, 'body')).toStrictEqual('')
})

test('Conversion of Simple JSON to CSS', () => {
  expect(
    jsonToCss(
      {
        'background': 'red',
        'color': 'green',
        'font-size': '16px',
      },
      '.magic',
    ),
  ).toStrictEqual('.magic{background:red;color:green;font-size:16px}')
})

test('Conversion of Nested JSON to CSS', () => {
  expect(
    jsonToCss(
      {
        'background': 'red',
        'color': 'green',
        'font-size': '16px',
        'div': {
          'font-weight': 'bold',
          'text-decoration': 'underline',
        },
      },
      '.magic',
    ),
  ).toStrictEqual(
    '.magic{background:red;color:green;font-size:16px}.magic div{font-weight:bold;text-decoration:underline}',
  )
})

test('Conversion of Complex JSON structure to CSS', () => {
  expect(
    jsonToCss(
      {
        header: {
          'width': '100%',
          '.section': {
            'background-color': 'red',
            '.primary': {
              'background-color': 'green',
              'h1': {
                'font-weight': 'bold',
              },
            },
          },
        },
      },
      '.magic',
    ),
  ).toStrictEqual(
    '.magic header{width:100%}.magic header .section{background-color:red}.magic header .section .primary{background-color:green}.magic header .section .primary h1{font-weight:bold}',
  )
})

test('Conversion of JSON with & modifier to CSS', () => {
  expect(
    jsonToCss(
      {
        'transition': 'background-color .5s ease-in-out',
        '&:hover': {
          'background-color': 'red',
        },
      },
      'h1',
    ),
  ).toBe('h1{transition:background-color .5s ease-in-out}h1:hover{background-color:red}')
})

test('Conversion of JSON with @media query to CSS', () => {
  expect(
    jsonToCss(
      {
        'margin': '0 auto',
        '@media only screen and (max-width: 600px)': {
          'max-width': '768px',
          '.row': {
            'display': 'flex',
            '.column': {
              display: 'block',
            },
          },
        },
      },
      '.container',
    ),
  ).toBe(
    '.container{margin:0 auto}@media only screen and (max-width: 600px){.container{max-width:768px}.container .row{display:flex}.container .row .column{display:block}}',
  )
})

test('Generates correct CSS from nested JSON with element selectors and pseudo-classes', () => {
  expect(
    jsonToCss({
      'input, button, textarea': {
        'background': 'lightgray',
        '.outlined': {
          'border': '1px solid gray',
          '&:focus': {
            'background-color': 'white',
            'border-color': 'black',
          },
        },
      },
    }),
  ).toBe(
    'input,button,textarea{background:lightgray}input,button,textarea .outlined{border:1px solid gray}input,button,textarea .outlined:focus{background-color:white;border-color:black}',
  )
})

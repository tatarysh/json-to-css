# @rysh/json-to-css

## Overview
`@rysh/json-to-css` is a powerful package that converts JSON objects into minified CSS code. This tool is designed to streamline the process of generating CSS from JSON structures, making it easier to manage styles programmatically.

## Installation
To install the package, use npm or yarn:
```sh
npm install @rysh/json-to-css
```
or
```sh
yarn add @rysh/json-to-css
```

## Usage
Below are examples demonstrating how to use `@rysh/json-to-css` in your project.

### Basic Usage
```typescript
import jsonToCss from "@rysh/json-to-css";

const css = jsonToCss({
  'background-color': '#fff',
  'color': '#000',
  'h1': {
    'font-weight': 'bold',
    '&.primary': {
      'color': '#12850f',
      '&:hover': {
        'background-color': '#ccc',
      }
    }
  }
}, '#app');

console.log(css);
```

### Output
The resulting CSS will be:
```css
#app{
    background-color:#fff;
    color:#000
}
#app h1{
    font-weight:bold
}
#app h1.primary{
    color:#12850f
}
#app h1.primary:hover{
    background-color:#ccc
}
```
The generated CSS is minified, removing all unnecessary spaces, commas, and semicolons.

### Handling Multiple Selectors
`@rysh/json-to-css` also supports selectors separated by commas:

```typescript
import jsonToCss from "@rysh/json-to-css";

const css = jsonToCss({
  'h1, h2, h3, h4, h5, h6': {
    'font-weight': 'bold',
    '&.none': {
      'display': 'none',
    }
  }
}, '#app');

console.log(css);
```

### Output
The resulting CSS will be:
```css
#app h1,h2,h3,h4,h5,h6{
    font-weight:bold
}
#app h1.none,h2.none,h3.none,h4.none,h5.none,h6.none{
    display:none
}
```

### Media Queries
`@rysh/json-to-css` supports media queries within the JSON structure:

```typescript
import jsonToCss from "@rysh/json-to-css";

const css = jsonToCss({
  '.container': {
    'margin': '0 auto',
    '@media only screen and (max-width: 600px)': {
      'max-width': '768px',
    }
  }
}, '#app');

console.log(css);
```

### Output
The resulting CSS will be:
```css
#app .container{
    margin:0 auto
}
@media only screen and (max-width: 600px){
    #app .container{
        max-width:768px
    }
}
```

### Combining Functions
You can mix various features to achieve the desired effect, giving you the flexibility to define complex styles using a straightforward JSON structure.

## License
This package is licensed under the MIT License.

## Contributions
Contributions, issues, and feature requests are welcome. Feel free to check the [issues page](https://gitlab.com/tatarysh/json-to-css/issues) if you have any suggestions or find any bugs.

## Author
Created by rysh.

---

This README file provides a comprehensive guide to using `@rysh/json-to-css`. If you have any further questions or need additional examples, please refer to the [documentation](https://gitlab.com/tatarysh/json-to-css).

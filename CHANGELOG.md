# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2024-07-28)


### Features

* implementation of the package's core functionality ([da9f128](https://tatarysh///commit/da9f12827b2c9e0e1021fd44c2ae97b9f49423ce))

## 1.0.0 (2024-07-28)


### Features

* implementation of the package's core functionality ([110a7a8](https://tatarysh///commit/110a7a8c9749df0f4bcb1811598638a27661c2a5))
